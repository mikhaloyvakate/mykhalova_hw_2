import User from '../models/User.js'
import Notes from '../models/Notes.js'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import notes from '../models/Notes.js'

export const getNotes = async (req, res) => {
    try {
        
        const userNotes = await Notes.find({userId: req.userId})
        
        if (!userNotes) {
            return res.status(404).json({
                message: 'This user does not have notes',
            })
        }
        res.status(200).json({
            offset:0,limit:0,count:userNotes.length-1,userNotes
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}

export const addNotes = async (req, res) => {
    try {

        const newNotes = new Notes({
            userId:req.userId,
            completed:false,
            text:req.body.text
        })
        await newNotes.save()
        
        res.status(200).json({
         message:  "Success"
        })
    } catch (error) {
        res.status(500).json({ message: 'Error' })
    }
}

export const updateMe = async (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body

        let user = await User.findById(req.userId);
        

        const salt = bcrypt.genSaltSync(10)
        const newhash = bcrypt.hashSync(newPassword, salt)
        
            user.update({
                password: newhash
            })

            res.status(200).json({
                message: "Success"
            })
        
    } catch (error) {
        res.status(500).json({ message: 'Error' })
    }
}

export const getNoteById = async (req, res) => {
    try {
        const userNotes = await Notes.findById(req.params.id)
        if (!userNotes) {
            return res.status(404).json({
                message: 'This note does not exist',
            })
        }
        res.status(200).json({
            userNotes
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}

export const deleteNoteById = async (req, res) => {
    try {
        const userNotes = await Notes.findById(req.params.id).remove();
        if (!userNotes) {
            return res.status(404).json({
                message: 'This user does not have notes',
            })
        }
        res.status(200).json({
            message: 'Success'
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}

export const putNoteById = async (req, res) => {
    try {
        const userNotes = await Notes.findByIdAndUpdate(req.params.id, { text: req.body.text })
       
        res.status(200).json({
            message: "Success"
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}

export const patchNoteById = async (req, res) => {
    try {
        const userNotes = await Notes.findByIdAndUpdate(req.params.id, { completed: !req.params.id.completed  })
        
        res.status(200).json({
            message: "Success"
        })
    } catch (error) {
        res.status(500).json({ message: 'No access.' })
    }
}
