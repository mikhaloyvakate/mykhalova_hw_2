import { Router } from 'express'
import { getNotes, addNotes,getNoteById , deleteNoteById, putNoteById,patchNoteById} from '../controllers/notes.js'
import { checkAuth } from '../utils/checkAuth.js'
const router = new Router()


router.get('/', checkAuth, getNotes)

router.post('/',checkAuth, addNotes)

router.get('/:id',checkAuth, getNoteById)

router.delete('/:id', checkAuth, deleteNoteById)

router.put('/:id', checkAuth, putNoteById)

router.patch('/:id',checkAuth, patchNoteById)



export default router